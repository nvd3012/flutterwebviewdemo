import 'package:get_it/get_it.dart';
import 'package:webviewdemo/core/services/api.dart';
import 'package:webviewdemo/core/services/authension_service.dart';
import 'package:webviewdemo/core/services/dialog_service.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/ui/views/home/home_viewmodel.dart';
import 'package:webviewdemo/ui/views/login/dialog_viewmodel.dart';
import 'package:webviewdemo/ui/views/login/login_viewmodel.dart';


GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory(() => LoginViewModel());
  locator.registerFactory(() => HomeViewModel());
  locator.registerFactory(() => DialogViewModel());

  locator.registerLazySingleton(() => SharedPreferencesService());
  locator.registerLazySingleton(()=>Api());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => DialogService());

}
