import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:webviewdemo/core/services/authension_service.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/route.dart';

bool _isAuthenticated = false;

void main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  _isAuthenticated = await authLogin();
  runApp(MyApp());
}

Future<bool> authLogin() async {
  final AuthenticationService auth = locator<AuthenticationService>();
  final SharedPreferencesService sharedPreferencesService =
      locator<SharedPreferencesService>();
  await sharedPreferencesService.init();
  String account = sharedPreferencesService.getData('account');
  log('account shared $account');
  if (account != null) {
    var userAccount = json.decode(account) as Map<String, dynamic>;
    var login = await auth.login(userAccount['email'], userAccount['password']);
    log('login $login');
    return login;
  }
  return false;
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          title: 'Webview Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          initialRoute: _isAuthenticated ? 'home' : '/',
          onGenerateRoute: Router.generateRoute,
        );
  }
}
