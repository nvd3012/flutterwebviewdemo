import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/views/base/base_viewmodel.dart';

class BaseView<T extends BaseViewModel> extends StatefulWidget {
  final Widget Function(BuildContext context, T value, Widget child) builder;
  final Function(T) onViewModelReady;
  final Function onDispose;

  BaseView({@required this.builder, this.onViewModelReady, this.onDispose});

  @override
  _BaseViewState<T> createState() => _BaseViewState<T>();
}

class _BaseViewState<T extends BaseViewModel> extends State<BaseView<T>> {
  T viewModel = locator<T>();


  @override
  void initState() {
    if (widget.onViewModelReady != null) {
      widget.onViewModelReady(viewModel);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create: (context) {
        return viewModel;
      },
      child: Consumer<T>(builder: widget.builder),
    );
  }



  @override
  void dispose() {
    if (widget.onDispose != null) {
      widget.onDispose();
    }
    super.dispose();
  }
}
