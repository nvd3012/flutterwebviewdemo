import 'package:flutter/material.dart';
import 'package:webviewdemo/core/enum/connect_state.dart';
import 'package:webviewdemo/core/enum/view_state.dart';
import 'package:webviewdemo/core/services/dialog_service.dart';
import 'package:webviewdemo/locator.dart';

class BaseViewModel extends ChangeNotifier{
  final DialogService _dialogService = locator<DialogService>();
  var _state;

  ViewState get state => _state;


  set state(ViewState value) {
    _state = value;
  }

  void setState(ViewState viewState){
    _state = viewState;
    notifyListeners();
  }
  void setStateNormal(){
    notifyListeners();
  }

  errorNetwork(ConnectivityStatus state) async {
    print('dialog shown $state');
    setStateNormal();
    if(state==ConnectivityStatus.Disconnected){
      await _dialogService.showDialog();
    }
    setStateNormal();
  }

}