import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webviewdemo/core/enum/view_state.dart';
import 'package:webviewdemo/core/services/authension_service.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/shared/constants.dart';
import 'package:webviewdemo/ui/views/base/base_viewmodel.dart';

class LoginViewModel extends BaseViewModel {
  final AuthenticationService _auth = locator<AuthenticationService>();
  final List<DropdownMenuItem<String>> _dropdownMenuItem = [
    'https://wwwdev-alb.qme.jp',
    'https://www.qme.jp',
    'https://wwwdev.qme.jp',
    'https://wwwdev2.qme.jp',
    'https://wwwdev3.qme.jp'
  ].map<DropdownMenuItem<String>>((String value) {
    return DropdownMenuItem<String>(
      value: value,
      child: Text(value),
    );
  }).toList();
  final SharedPreferencesService _sharedPreferencesService =
      locator<SharedPreferencesService>();
  String _dropDownValue;

  String get dropDownValue => _dropDownValue;

  List<DropdownMenuItem<String>> get dropdownMenuItem => _dropdownMenuItem;

  String errorMessage;

  void setSelectedDropdown(val) {
    _dropDownValue = val;
    _sharedPreferencesService.setData(SERVER, val);
    setStateNormal();
  }

  void initServer(){
    _sharedPreferencesService.setData(SERVER, dropdownMenuItem.elementAt(0).value);
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      return 'Value entered is not empty';
    }
    return '';
  }

  void onStarted(String url) {
    log('onStarted Page');
  }

  void onError(WebResourceError error) {}

  Future<bool> login(String email, String password) async {
    setState(ViewState.Busy);
//    if(email.isEmpty||password.isEmpty){
//      errorMessage = 'Value entered is not a number';
//      return false;
//    }
    var success = await _auth.login(email.trim(), password.trim());
    if (!success) {
      errorMessage = 'Email or password invalid!';
      setState(ViewState.Idle);
    }
    log('login $success');
    return success;
  }
}
