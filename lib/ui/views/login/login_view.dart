import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webviewdemo/core/enum/view_state.dart';
import 'package:webviewdemo/ui/shared/ui_helpers.dart';
import 'package:webviewdemo/ui/views/base/base_view.dart';
import 'package:webviewdemo/ui/views/login/dialog_reset_password.dart';
import 'package:webviewdemo/ui/views/login/login_viewmodel.dart';
import 'package:webviewdemo/ui/widgets/login_components.dart';

class LoginView extends StatelessWidget {
  final TextEditingController _controllerEmail = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(
      onViewModelReady: (viewModel) {
        viewModel.initServer();
      },
      builder: (context, viewModel, child) => SafeArea(
          child: Scaffold(
            resizeToAvoidBottomPadding: false,
        body:Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(20),
                padding: EdgeInsets.all(10),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.green),
                child: Icon(
                  Icons.person,
                  size: 60,
                  color: Colors.white,
                ),
              ),
              Text(
                '出店主ログイン画面',
                style: TextStyle(fontSize: 18.0),
              ),
              LoginTextField(
                  _controllerEmail, viewModel.validateEmail, 'Email', false),
              LoginTextField(_controllerPassword, viewModel.validateEmail,
                  'Password', true),
              UIHelper.verticalSpaceSmall(),
              GestureDetector(
                child: Text(
                  'PASSWORDをお忘れですか?',
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.green,
                  ),
                ),
                onTap: () {
                  showGeneralDialog(
                    context: context,
                    barrierColor: Colors.white,
                    // background color
                    barrierDismissible: false,
                    // should dialog be dismissed when tapped outside
                    barrierLabel: "Dialog",
                    // label for barrier
                    transitionDuration: Duration(milliseconds: 400),
                    // how long it takes to popup dialog after button click
                    pageBuilder: (_, __, ___) {
                      // your widget implementation
                      return DialogResetPassword();
                    },
                  );
                },
              ),
              UIHelper.verticalSpaceSmall(),
              viewModel.errorMessage != null
                  ? Text(
                      '${viewModel.errorMessage}',
                      style: TextStyle(color: Colors.red),
                    )
                  : Container(),
              viewModel.state == ViewState.Busy
                  ? CircularProgressIndicator()
                  : Container(
                      margin:
                          EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                      child: MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        minWidth: double.infinity,
                        child: Text('Login',
                            style: TextStyle(
                              fontSize: 18.0,
                            )),
                        textColor: Colors.white,
                        color: Colors.green,
                        onPressed: () async {
                          bool loginSuccess = await viewModel.login(
                              _controllerEmail.text, _controllerPassword.text);
                          if (loginSuccess) {
                            Navigator.popAndPushNamed(context, 'home');
                          }
                        },
                      )),
              Text(
                'Choose server:',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
              DropdownButton<String>(
                value: viewModel.dropDownValue == null
                    ? viewModel.dropdownMenuItem.elementAt(0).value
                    : viewModel.dropDownValue,
                icon: Icon(Icons.arrow_downward),
                iconSize: 24,
                elevation: 16,
                style: TextStyle(color: Colors.green),
                onChanged: (String newValue) {
                  viewModel.setSelectedDropdown(newValue);
                },
                items: viewModel.dropdownMenuItem,
              ),
            ],
          ),
      )),
    );
  }
}
