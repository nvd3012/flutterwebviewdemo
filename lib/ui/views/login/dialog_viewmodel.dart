import 'dart:developer';

import 'package:webviewdemo/core/enum/view_state.dart';
import 'package:webviewdemo/core/services/api.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/views/base/base_viewmodel.dart';

class DialogViewModel extends BaseViewModel {
  final Api _api = locator<Api>();
  String _resetPasswordError = '';

  String get resetPasswordError => _resetPasswordError;

  Future resetPassword(String email) async {
    setState(ViewState.Busy);
    var resetResult = await _api.resetPassword(email);
    log('resetPassword $resetResult');
    if (resetResult == false) {
      _resetPasswordError = 'Email invalid !';
      setState(ViewState.Idle);
    } else {
      setState(ViewState.Success);
    }

  }
}
