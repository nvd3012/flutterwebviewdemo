import 'package:flutter/material.dart';
import 'package:webviewdemo/core/enum/view_state.dart';
import 'package:webviewdemo/ui/shared/ui_helpers.dart';
import 'package:webviewdemo/ui/views/base/base_view.dart';
import 'package:webviewdemo/ui/views/login/dialog_viewmodel.dart';
import 'package:webviewdemo/ui/widgets/login_components.dart';

class DialogResetPassword extends StatefulWidget {
  @override
  createState() => _DialogResetPasswordState();
}

class _DialogResetPasswordState extends State<DialogResetPassword> {
  final TextEditingController _controllerEmail = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BaseView<DialogViewModel>(
      builder: (context, viewModel, child) => SizedBox.expand(
          // makes widget fullscreen
          // makes widget fullscreen
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.green,
                title: Text('Reset password'),
                leading: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              body: viewModel.state == ViewState.Success
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15.0, vertical: 15.0),
                          margin: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 20.0),
                          decoration: BoxDecoration(
                              color: Colors.greenAccent,
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                  width: 1, color: Colors.lightGreenAccent)),
                          child: Text(
                            'パスワード再設定用のメールを送信致しました。',
                            style: TextStyle(fontSize: 18),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        MaterialButtonInApp(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          text: 'Close',
                        )
                      ],
                    )
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        LoginTextField(
                            _controllerEmail, {}, 'メールアドレスを入力してください。', false),
                        Visibility(
                          visible: viewModel.resetPasswordError.isNotEmpty,
                          child: Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 10.0),
                            child: Text(
                              viewModel.resetPasswordError,
                              style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        UIHelper.verticalSpace(5.0),
                        viewModel.state == ViewState.Busy
                            ? CircularProgressIndicator()
                            : MaterialButtonInApp(
                                onPressed: () {
                                  viewModel
                                      .resetPassword(_controllerEmail.text);
                                },
                                text: '送信',
                              )
                      ],
                    ))),
    );
  }
}
