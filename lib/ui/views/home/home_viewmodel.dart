import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webviewdemo/core/enum/view_state.dart';
import 'file:///D:/Flutter/webview_demo/lib/ui/widgets/web_view.dart';
import 'package:webviewdemo/ui/views/base/base_viewmodel.dart';


class HomeViewModel extends BaseViewModel {
  int _selectedIndex = 0;
  WebViewController _controller;

  logout() {}


  int get selectedIndex => _selectedIndex;


  Future onItemTapped(int index, Widget elementAt) async {
    if(elementAt is WebViewContainer){
      _controller = await elementAt.controller.future;
    }
    print('select item $index');
    _selectedIndex = index;
    setState(ViewState.Idle);
  }

  Future<bool> onBack(BuildContext context) async {
    if (await _controller.canGoBack()) {
      await _controller.goBack();
      return false;
    }
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit an App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => {Navigator.of(context).pop(false)},
                child: Text('No'),
              ),
              FlatButton(
                onPressed: () => exit(0),
                child: Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }
}
