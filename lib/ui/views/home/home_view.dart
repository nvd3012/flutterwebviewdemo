import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webviewdemo/core/services/authension_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/views/base/base_view.dart';
import 'package:webviewdemo/ui/views/home/home_viewmodel.dart';
import 'file:///D:/Flutter/webview_demo/lib/ui/widgets/web_view.dart';

class HomeView extends StatelessWidget {
  final AuthenticationService _authenticationService = locator<AuthenticationService>();

  @override
  Widget build(BuildContext context) {
    final List<Widget> _children = [
      WebViewContainer(
        key: UniqueKey(),
        url: "https://wwwdev-alb.qme.jp/kitchen/events",
      ),
      WebViewContainer(
        key: UniqueKey(),
        url: "https://wwwdev-alb.qme.jp/kitchen/profile",
      ),
      Center(
        child: MaterialButton(
          onPressed: () => {
            _authenticationService.clearAuth(),
            Navigator.popAndPushNamed(context,'/')
          },
          color: Colors.green,
          textColor: Colors.white,
          child: Text(
            'Logout',
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
    ];
    return BaseView<HomeViewModel>(
        onViewModelReady: (viewModel) => {
        },
        builder: (context, viewModel, child) => WillPopScope(
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: Colors.white,
                  drawer: Drawer(
                    child: ListView(
                      children: <Widget>[
                        DrawerHeader(
                          child: Text('Drawer Title'),
                          decoration: BoxDecoration(color: Colors.blue),
                        ),
                        Text('Item 1'),
                        Text('Item 2'),
                        Text('Item 3'),
                        Text('Item 4'),
                        Text('Item 5'),
                      ],
                    ),
                  ),
                  body: IndexedStack(
                    index: viewModel.selectedIndex,
                    children: _children,
                  ),
                  bottomNavigationBar: BottomNavigationBar(
                    backgroundColor: Colors.white,
                    elevation: 10,
                    items: const <BottomNavigationBarItem>[
                      BottomNavigationBarItem(
                        icon: Icon(Icons.search),
                        title: Text('Search'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.person),
                        title: Text('Info'),
                      ),
                      BottomNavigationBarItem(
                        icon: Icon(Icons.settings),
                        title: Text('Settings'),
                      ),
                    ],
                    currentIndex: viewModel.selectedIndex,
                    selectedItemColor: Colors.green,
                    unselectedItemColor: Colors.black45,
                    onTap: (index) => viewModel.onItemTapped(
                        index, _children.elementAt(index)),
                  ),
                ),
              ),
              onWillPop: () {
                return viewModel.onBack(context);
              },
            ));
  }
}
