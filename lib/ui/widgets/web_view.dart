import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webviewdemo/core/services/authension_service.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/shared/constants.dart';

class WebViewContainer extends StatefulWidget {
  final Function(String) onPageFinished;
  final Function(String) onPageStarted;
  final Function(WebResourceError) onWebResourceError;
  final url;
  final key;

  final Completer<WebViewController> controller =
      Completer<WebViewController>();

  WebViewContainer(
      {this.onPageFinished,
      this.onPageStarted,
      this.onWebResourceError,
      @required this.url,
      @required this.key});

  @override
  createState() => _WebviewContainerState();
}

class _WebviewContainerState extends State<WebViewContainer> {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  final SharedPreferencesService _sharedPreferencesService =
      locator<SharedPreferencesService>();
  WebViewController _controller;
  final Map<String, String> headers = {};
  bool _isVisible = true;

  void _changeState(state) {
    setState(() {
      _isVisible = state;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        WebView(
          key: widget.key,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (webController) async {
            _controller = webController;
            widget.controller.complete(webController);
            headers[HTTP_COOKIE] =
                _sharedPreferencesService.getData(HTTP_COOKIE);
            webController.loadUrl(widget.url, headers: headers);
          },
          onPageStarted: (url) {
            log('started link $url');
//            if(url=='https://wwwdev-alb.qme.jp/logout'){
//              _authenticationService.clearAuth();
//              Navigator.popAndPushNamed(context, '/');
//            }
            _changeState(true);
            widget.onPageStarted(url);
          },
          onPageFinished: (url) {
            if (url == 'https://wwwdev-alb.qme.jp/') {
              _authenticationService.clearAuth();
              Navigator.popAndPushNamed(context, '/');
            }
            _changeState(false);
            widget.onPageFinished(url);
          },
          onWebResourceError: (error) {
            _changeState(false);
            widget.onWebResourceError(error);
          },
          gestureRecognizers: Platform.isAndroid
              ? {Factory(() => EagerGestureRecognizer())}
              : null,
        ),
        Visibility(
          child: Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
          visible: _isVisible,
        )
      ],
    );
  }

  @override
  void dispose() {
    log('dispose webview');
    _WebviewContainerState();
    super.dispose();
  }

}
