import 'package:flutter/material.dart';

class MaterialButtonInApp extends StatelessWidget{
  final Function() onPressed;
  final text;

  MaterialButtonInApp({@required this.onPressed,this.text});

  @override
  Widget build(BuildContext context) {
    return  MaterialButton(
        color: Colors.green,
        textColor: Colors.white,
        child: Text(
          text,
          style: TextStyle(fontSize: 18),
        ),
        onPressed: () => {
          onPressed()
        });
  }

}

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;
  final hintText;
  final validate;
  final typePassword;

  LoginTextField(this.controller,this.validate,this.hintText,this.typePassword);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15.0),
      margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      height: 50.0,
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(width: 1, color: Colors.grey)
      ),
      child: TextField(
        obscureText: typePassword,
          decoration: InputDecoration(
            border: InputBorder.none,
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            hintText: hintText,
//            suffixIcon: Icon(Icons.error, color: Colors.red),
          ),
//          validator: validate,
          controller: controller),
    );
  }
}