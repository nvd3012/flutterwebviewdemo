import 'package:flutter/material.dart';
import 'package:webviewdemo/ui/views/home/home_view.dart';
import 'package:webviewdemo/ui/views/login/login_view.dart';

class Router{
  static Route<dynamic> generateRoute(RouteSettings settings){
    print("route ${settings.name}");
    switch(settings.name){
      case "/":
        return MaterialPageRoute(builder: (_)=> LoginView());
      case "home":
        return MaterialPageRoute(builder: (_)=> HomeView());
      default:
        return MaterialPageRoute(builder: (_){
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}