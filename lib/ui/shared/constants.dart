
const String HTTP_COOKIE = "cookie";
const String HTTP_CONTENT_TYPE = "content-type";
const String HTTP_LOCATION = "location";
const String ACCOUNT = "account";
const String EMAIL = "email";
const String PASSWORD = "password";
const String SERVER = "SERVER";
const String QME = "https://www.qme.jp";
const String DEV = "https://wwwdev-alb.qme.jp";