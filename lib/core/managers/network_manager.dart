import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webviewdemo/core/enum/connect_state.dart';

class NetworkSensitive extends StatefulWidget {
  final Widget child;

  NetworkSensitive({this.child});

  @override
  createState() => _NetworkSensitiveState();
}

class _NetworkSensitiveState extends State<NetworkSensitive> {
  @override
  Widget build(BuildContext context) {
    var connectionStatus = Provider.of<ConnectivityStatus>(context);
    if (connectionStatus == ConnectivityStatus.Disconnected) {
      setState(() {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure?'),
            content: Text('Do you want to exit an App'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => exit(0),
                child: Text('Exit'),
              ),
              FlatButton(
                onPressed: () => {Navigator.of(context).pop(false)},
                child: Text('Try again!'),
              ),
            ],
          ),
        );
      });
    }
    return widget.child;
  }
}
