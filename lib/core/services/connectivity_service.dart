import 'dart:async';
import 'dart:developer';

import 'package:connectivity/connectivity.dart';
import 'package:webviewdemo/core/enum/connect_state.dart';

class ConnectivityService {
  StreamController<ConnectivityStatus> connectionStatusController =
      StreamController<ConnectivityStatus>();

  ConnectivityService() {
    Connectivity().onConnectivityChanged.listen((event) {
      log('connect change ${_getStatusFromResult(event)}');
      connectionStatusController.add(_getStatusFromResult(event));
    });
  }

  ConnectivityStatus _getStatusFromResult(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.none:
        return ConnectivityStatus.Disconnected;
      default:
        return ConnectivityStatus.Connecting;
    }
  }

}
