import 'package:webview_flutter/webview_flutter.dart';

class WebViewServices {
  final WebViewController controller;
  final CookieManager cookieManager = CookieManager();

  WebViewServices(this.controller);



  Future<String> onShowUserAgent() async {
    return await controller.evaluateJavascript('navigator.userAgent;');
  }

  Future<String> executeScript(String script) async {
    return await controller.evaluateJavascript(script);
  }

  void setLocalStorage(String key, String value) async {
    await controller
        .evaluateJavascript('window.localStorage.setItem("$key","$value");');
  }

  Future<String> getLocalStorage(String key) async {
    return await controller
        .evaluateJavascript('window.localStorage.getItem("$key");');
  }

  void onClearCache() async {
    await controller.clearCache();
  }

  void onClearCookies() async {
    final bool hadCookies = await cookieManager.clearCookies();
    String message = 'There were cookies. Now, they are gone!';
    if (!hadCookies) {
      message = 'There are no cookies.';
    }
  }

  Future<String> getCookies() async {
    final String cookies =
        await controller.evaluateJavascript('document.cookie');
    if (cookies == null || cookies == '""') {
      return "";
    }
    return cookies;
  }

  void onSetCookies(String cookie) async {
    await controller.evaluateJavascript('document.cookie = "$cookie"');
  }
}
