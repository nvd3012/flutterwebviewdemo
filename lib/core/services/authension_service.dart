import 'dart:developer';

import 'package:webview_flutter/webview_flutter.dart';
import 'package:webviewdemo/core/services/api.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/shared/constants.dart';

class AuthenticationService {
  final Api _api = locator<Api>();
  final SharedPreferencesService _sharedPreferencesService =
      locator<SharedPreferencesService>();

  Future<String> checkCookie() async{
    var cookie = _sharedPreferencesService.getData(HTTP_COOKIE);
    _api.setCookie(cookie);
    var resultCheck = await _api.getLoginPage();
    if(resultCheck.statusCode!=302){

    }
    return '';
  }

  clearAuth(){
    CookieManager().clearCookies();
    _api.clearCookie();
    _sharedPreferencesService.sharedPreferences.clear();
  }

  Future<bool> login(String email, String password) async {
    var resultLogin = await _api.login(email, password);
    log('resultLogin $resultLogin');
    if (resultLogin.statusCode == 302) {
      if (resultLogin.headers['location']
          .elementAt(0)
          .contains('kitchen/client')) {
        var userAccount =
            '{"email":"${email.trim()}","password":"${password.trim()}"}';
        _sharedPreferencesService.setData('account', userAccount);
        var cookie = _api.getCookie();
        cookie.remove('content-type');
        log('cookie shared ${cookie['cookie']}');
        _sharedPreferencesService.setData('cookie', cookie['cookie']);
        return true;
      }
    }
    return false;
  }
}
