import 'dart:async';

class DialogService{
  Function _showDialogListener;
  Completer _dialogCompleter;

  registerDialogListener(Function showDialogListener){
    _showDialogListener = showDialogListener;
  }

  Future showDialog(){
    _dialogCompleter = Completer();
    _showDialogListener();
    return _dialogCompleter.future;
  }

  dialogComplete(){
    _dialogCompleter.complete();
    _dialogCompleter = null;
  }

}