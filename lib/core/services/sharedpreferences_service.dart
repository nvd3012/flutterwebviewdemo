import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesService {
  SharedPreferences sharedPreferences;

  init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  setData(String key, String data) {
    sharedPreferences.setString(key, data);
  }

  String getData(String key) {
    return sharedPreferences.getString(key);
  }
}
