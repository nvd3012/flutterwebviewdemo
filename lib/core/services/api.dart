import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:webviewdemo/core/services/sharedpreferences_service.dart';
import 'package:webviewdemo/locator.dart';
import 'package:webviewdemo/ui/shared/constants.dart';

class Api {
  var _session = Session();

  Map<String, String> getCookie() {
    return _session._cookie;
  }

  void setCookie(String cookie) {
    _session._cookie[HTTP_COOKIE] = cookie;
  }

  void clearCookie() {
    _session._cookie = {};
  }

  Future<Response> getLoginPage() async {
    return await _session.get('/kitchen/login');
  }

  Future<String> getTokenAndCookiesPage() async {
    log('loginServer');
    var response = await _session.get('/kitchen/login');
    var body = response.data.substring(response.data.indexOf('_token') + 10);
    int startIndex = body.indexOf('"') + 1;
    int endIndex = body.indexOf('"', startIndex);
    var token = body.substring(startIndex, endIndex);
    log('login token $token');
    return token;
  }

  Future<bool> resetPassword(String email) async {
    var token = await getTokenAndCookiesPage();
    Map<String, dynamic> data = {'_token': '$token', 'email': '$email'};
    var response = await _session.post('/password/email', data,
        'multipart/form-data; boundary=----WebKitFormBoundaryzGy4EwXjpBcbj1C9');
    log('reset password ${response.data}');
    return response.data.toString().isEmpty;
  }

  Future<Response> login(String email, String password) async {
    log('encode account $email and $password');
    var token = await getTokenAndCookiesPage();
    Map<String, dynamic> body = {
      '_token': '$token',
      'email': '$email',
      'password': '$password'
    };
    var response = await _session.post(
        '/kitchen/login', body, Headers.formUrlEncodedContentType);
    log('response post ${response.statusCode} and ${response.data}');
    return response;
  }
}

class Session {
  final SharedPreferencesService _sharedPreferencesService =
      locator<SharedPreferencesService>();
  var requestHttp = Dio();
  Map<String, String> _cookie = {};

  //  static const endpoint = 'https://wwwdev-alb.qme.jp';
  var endpoint = 'https://wwwdev-alb.qme.jp';

  void getEndPoint() {
    endpoint = _sharedPreferencesService.getData(SERVER);
  }

  Future<Response> get(String url) async {
    getEndPoint();
    Response response = await requestHttp.get('$endpoint$url̉',
        options: Options(headers: _cookie));
    updateCookie(response);
    return response;
  }

  Future<Response> post(String url, dynamic data, String contentType) async {
    getEndPoint();
    _cookie[HTTP_CONTENT_TYPE] = contentType;
    Response response = await requestHttp.post('$endpoint$url',
        data: data,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            },
            headers: _cookie));
    updateCookie(response);
    return response;
  }

  void updateCookie(Response response) {
    var resultCookie = response.headers.map['set-cookie'].toString();
    String rawCookie = resultCookie.substring(1, resultCookie.length - 1);
    log('cookies save $_cookie');
    if (rawCookie != null) {
      var cookie1 = rawCookie.substring(0, rawCookie.indexOf(';'));
      var indexCookie2 = rawCookie.indexOf('qme_session');
      var cookie2 = rawCookie.substring(
          indexCookie2, rawCookie.indexOf(';', indexCookie2));
      _cookie[HTTP_COOKIE] = '$cookie1; $cookie2';
      log('cookies save $_cookie');
    }
  }
}
